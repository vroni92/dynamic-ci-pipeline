# gitlab-dynamic-ci-pipelines

This repo is a *template*. You must make changes as specified in this document in order to use it in your repo.

## Overview
This repo guides you through creating custom, dynamic Gitlab CICD pipelines. It is intended to be added to and modified to implement the functionality needed for specific CICD use cases.

## Generating the Dynamic Pipeline
This repo runs CI pipelines on either `Merge Request` events, commits to the default branch (`main`, `master`, etc), or manual pipelines (kicked off in the Gitlab UI). Each of these pipelines are dynamically generated (see next section).

Each pipeline consists of two stages: `generate-pipeline` > `run-pipeline`.

### Stage: Generate Pipeline
This job runs a `python` script (**[generate-pipeline.py](./gitlab-ci/generate-pipeline.py)**) to dynamically create a downstream pipeline based off of the current branch and changes made in the commit. The pipeline is generated off of the template **[pipeline-template.j2](./gitlab-ci/pipeline-template.j2)**.

### Stage: Run Pipeline
This job kicks off the downstream pipeline generated in the `generate-pipeline` stage above. This pipeline will include whatever customized stages/jobs you added to the **[pipeline-template.j2](./gitlab-ci/pipeline-template.j2)** template.

## Customizing the Dynamic Pipeline
The downstream pipeline created in the `Generage Pipeline` stage and kicked off in the `Run Pipeline` stage can be modified to suit the needs of your use case. The following is the base configuration set up in this repo.

### Triggers
#### On Changes
The **[generate-pipeline.py](./gitlab-ci/generate-pipeline.py)** is designed to run a `git diff` on the current branch compared to the default branch (`main`, `master`, `develop`, etc). This default branch is taken from the environment variable `CI_DEFAULT_BRANCH`, which is available on all Gitlab Runners. If this environment variable is not available, it defaults to `main`.

Once the git diff is complete, the script uses the environment variable `BASE_DIRECTORY` to search for all "projects" (one directory level deeper) that have changed on this commit. If not specified, the base directory is set to `projects`.

For each "project" that changes under the base directory, that "project name" (name of the subdirectory) is passed as a config value to render the **[pipeline-template.j2](./gitlab-ci/pipeline-template.j2)** template (see the next section for templating details).

>**NOTE**: This can be customized/added to depending on your use case. Check out the **[generate-pipeline.py `get_changed_projects`](./gitlab-ci/generate-pipeline.py)** function for more details.

For example, you have the following directory structure, with `BASE_DIRECTORY` set to `tests`:
```tree
tests/
  test1/
    file1.txt
  test2/
    file2.txt
```

You create a feature branch, make a change to `tests/test2/file2.txt`, commit the change and open a Merge Request. The **[generate-pipeline.py](./gitlab-ci/generate-pipeline.py)** will find that `tests/test2/file2.txt` has changed, and pass `test2` as a config value to render the **[pipeline-template.j2](./gitlab-ci/pipeline-template.j2)** template.

#### Manually
The **[.gitlab-ci.yml](.gitlab-ci.yml)** has `manual` jobs for each of the two stages. This allows you to trigger a pipeline by navigating to `CICD` > `Pipelines` > `Run Pipeline`. Set the variables `DIRECTORIES` to a **space separated list** of "projects" you want to trigger. This will pass in the `--projects <names of projects>` flag to the **[generate-pipeline.py](./gitlab-ci/generate-pipeline.py)** script, allowing you to manually kick off individual projects without making changes to them. See the CI yaml below for details:
```yaml
generate-pipeline-manual:
  stage: generate-pipeline
  extends:
    - .pip3 requirements
  script:
    - git fetch
    - python3 gitlab-ci/generate-pipeline.py --projects $DIRECTORIES
  artifacts:
    paths:
      - gitlab-ci/generated-pipeline.yml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always
    - when: never

run-pipeline-manual:
  stage: run-pipeline
  trigger:
    include:
      - artifact: gitlab-ci/generated-pipeline.yml
        job: generate-pipeline-manual
    strategy: depend
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always
    - when: never
```

### Template CI YAML
Now that the script has captured all of the `configs` to pass in, it uses those configs to render the **[pipeline-template.j2](./gitlab-ci/pipeline-template.j2)** template. Specifically, it passes in the list of configs as follows:
```python
with open("gitlab-ci/pipeline-template.j2") as f:
    t = Template(f.read())
    render = t.render(configs=changed_projects)
    with open("gitlab-ci/generated-pipeline.yml", "w") as file:
        file.write(render)
```

You can see above that the `changed_projects` are passed in with the value name `configs`. This is how they are referenced in the **[pipeline-template.j2](./gitlab-ci/pipeline-template.j2)** file.

In the base template, each config project passed in will have a `job1` and `job2` created, as shown below:
```yaml
{% for config in configs %}
job1-{{ config }}:
  stage: stage1
  ...
job2-{{ config }}:
  stage: stage2
  ...
{% endfor %}
```

The template uses Jinja2 for rendering, so you can add additional or more complex logic to create your desired pipelines.

#### Include local CI YAML
In addition to the Gitlab CI configuration defined in the  **[pipeline-template.j2](./gitlab-ci/pipeline-template.j2)** file, it includes local reference to the **[ci-template.yml](./gitlab-ci/ci-template.yml)** file:
```yaml
include:
  - local: gitlab-ci/ci-template.yml
```

This allows you to define default configuration for all pipelines, both the parent pipeline and downstream generated pipeline. This may be useful for adding specific environment variables, runner images, or tags.

### Run jobs
Now that the pipeline has been configuration, the last step is defining what each job should run. The base template uses `Makefile` to simplify the commands defined the CI YAML itself. You can see that each job runs it's own makefile command, and can pass in values specific to it's configuration:
```yaml
job2-{{ config }}:
  stage: stage2
  script:
    - make stage2 MY_INPUT={{ config }}
  ...
```

This **[Makefile](Makefile)** lives at the top level directory of this repo. Here you can add the functionality your jobs need to run.
