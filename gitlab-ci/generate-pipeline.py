import os
import argparse
import git
from jinja2 import Template

CI_COMMIT_BRANCH = os.getenv("CI_COMMIT_BRANCH")


def get_branch_projects_diff(branch_one, branch_two, project_dir):
    """
    Runs a git diff between two branches to get changed files
    params:
        branch_one (i.e. main)
        branch_two (i.e. HEAD)
    returns:
        files - The changed files found in git diff under the docker/ directory.
                Used to calculate changed docker.
    """
    files = []
    repo = git.Git(os.getcwd())
    diff = repo.diff(
        "{}..{}".format(branch_one, branch_two), diff_filter="d", name_only=True
    ).split("\n")
    for line in diff:
        if "{}/".format(project_dir) in line:
            files.append(line)
    return files


def get_changed_projects(base_branch, main_flag, projects_dir):
    """Calculate the ${projects_dir}/* directories with changed files."""
    if main_flag:
        base = "HEAD^"
    else:
        base = f"origin/{base_branch}"
    print(f"base is {base}")
    files = get_branch_projects_diff(base, "HEAD", projects_dir)
    changed_projects = []

    # Find project directories
    for file in files:
        project = file.rsplit("/")[1]
        project_name = f"{project}"
        if project_name not in changed_projects:
            changed_projects.append(project_name)
    changed_projects.sort()
    return changed_projects


def generate_pipeline(changed_projects):
    with open("gitlab-ci/pipeline-template.j2") as f:
        t = Template(f.read())
        render = t.render(configs=changed_projects)
        with open("gitlab-ci/generated-pipeline.yml", "w") as file:
            file.write(render)
    print("Created gitlab-ci/generated-pipeline.yml!")


def main(args):
    base_branch = os.environ.get("CI_DEFAULT_BRANCH", "main")
    base_directory = os.environ.get("BASE_DIRECTORY", "projects")

    # Get changed projects
    if not args.projects:
        changed_projects = get_changed_projects(
            base_branch, args.main, base_directory)
    else:
        changed_projects = args.projects

    # Print configs...
    print(f"Project configs: {changed_projects}")

    # Generate pipeline
    generate_pipeline(changed_projects)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Parse generate-pipeline.py inputs"
    )
    parser.add_argument(
        "--on-default-branch",
        dest="main",
        action="store_true",
        help="Generate git diff from current and previous main commits",
        default=False,
    )
    parser.add_argument(
        "--projects",
        dest="projects",
        nargs="+",
        help="(Optional) Specify projects manually rather than using git diff",
        required=False,
    )
    args = parser.parse_args()
    main(args)
