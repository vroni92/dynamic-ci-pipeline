import os
import copy
import boto3
import pprint
from botocore.config import Config
from boto3.dynamodb.conditions import Attr
from botocore.exceptions import ClientError
from collections import OrderedDict
from datetime import datetime, timezone


DYNAMODB_TABLE = "my-dynamo-table"
DATETIME_FMT = "%m/%d/%Y, %H:%M:%S UTC"
OUTFILE_TMPL = "gitlab-ci/readme-template.md"
OUTFILE = "README.md"


def get_items(table, filter_key="chart", filter_value=None):
    if filter_value:
        responses = table.scan(
            FilterExpression=Attr(filter_key).eq(filter_value))
    else:
        responses = table.scan()
    data = responses['Items']
    while 'LastEvaluatedKey' in responses:
        response = table.scan(
            ExclusiveStartKey=responses['LastEvaluatedKey'])
        data.extend(response['Items'])
    return data


def delete_item(table, id):
    try:
        print(f"Deleting: {id}")
        response = table.delete_item(Key={'id': id})
    except ClientError as e:
        if e.response['Error']['Code'] == "ConditionalCheckFailedException":
            print(e.response['Error']['Message'])
        else:
            raise
    else:
        return response


def get_charts(items):
    chart_dict = {}
    for item in items:
        # TO DO: might have to acconut for multiple versions with same createtime
        if item["chart"] in chart_dict.keys():
            chart_dict[item["chart"]][item["createTime"]] = item["id"]
        else:
            chart_dict[item["chart"]] = {item["createTime"]: item["id"]}
    return chart_dict


def get_removal_items(versions, max):
    datetimes_cleanup = sorted(
        [datetime.strptime(date_str, DATETIME_FMT) for date_str in versions])[max:]
    ids_cleanup = [versions[date.strftime(DATETIME_FMT)]
                   for date in datetimes_cleanup]
    return ids_cleanup


def cleanup_versions(args):
    chart_list = args.chart_list.split(',')
    max = args.max
    print('chart_list:', chart_list)
    print('max:', max)

    # Define configs
    aws_config = Config(region_name="us-east-1")

    # Get dynamo table
    ddb = boto3.resource('dynamodb', config=aws_config)
    chart_table = ddb.Table(DYNAMODB_TABLE)

    # Get all items
    items = get_items(chart_table)
    # print(items)

    # Categorize items by chart
    chart_list = get_charts(items)

    # Get list of items (versions) to remove
    remove_list = []
    for chart, versions in chart_list.items():
        remove_list += get_removal_items(versions, max)

    # Remove old chart versions
    print("Chart versions to delete: {}".format(len(remove_list)))
    for id in remove_list:
        delete_item(chart_table, id)


def print_row(file, row_list, header=False):
    file.write('| ' + ' |'.join(row_list) + ' |\n')
    if header:
        print_row(file, [":---"] * len(row_list))


def get_table_data(header, items):
    data = []
    for key in header:
        if key == 'version':
            print(f"version found is: {items[key]}")
            if 'CI_COMMIT_SHORT_SHA' in items:
                data.append("[%s](https://gitlab.bit9.local/production-engineering/observability/helm-charts/-/commits/%s)" %
                            (items[key], items['CI_COMMIT_SHORT_SHA']))
            else:
                data.append(items[key])
        elif key == 'CI_PIPELINE_ID':
            data.append("[%s](%s)" %
                        (items['CI_PIPELINE_ID'], items['CI_PIPELINE_URL']))
        # elif key == 'CI_COMMIT_SHORT_SHA':
        #     data.append("[%s](https://gitlab.bit9.local/production-engineering/observability/helm-charts/-/commits/%s)" %
        #                 (items['CI_COMMIT_SHORT_SHA'], items['CI_COMMIT_SHORT_SHA']))
        else:
            try:
                data.append(items[key])
            except:
                data.append('')
    return data


def render_versions(args):
    chart_list = args.chart_list.split(',')
    limit = args.limit
    print('chart_list:', chart_list)
    print('limit:', limit)

    # Define configs
    aws_config = Config(region_name="us-east-1")

    # Get dynamo table
    ddb = boto3.resource('dynamodb', config=aws_config)
    chart_table = ddb.Table(DYNAMODB_TABLE)

    header_dict = OrderedDict({
        'version': 'Version',
        'CI_PIPELINE_ID': 'Pipeline',
        'CI_COMMITTER': 'Committer',
        'CI_COMMIT_TITLE': 'Title',
        'createTime': 'Create Time'
    }
    )

    # Create data output
    file_data = file_data2 = ""
    tmp_outfile = "tmp-%s" % OUTFILE

    # Reading data from outfile template
    with open(OUTFILE_TMPL) as fp:
        file_data = fp.read()

    with open(tmp_outfile, 'w') as f:
        f.write('Last update  at: %s\n\n' %
                datetime.now().strftime("%Y-%m-%d %H:%M utc"))

        ## print release table
        col = len(chart_list)
        version_list = []
        chart_list1 = copy.deepcopy(chart_list)
        while len(chart_list1) > 0:
            sublist = [' ']*col
            for i in range(col):
                try:
                    chart = chart_list1.pop(0)
                    sublist[i] = "[%s](#%s)" % (chart, chart)
                except:
                    break
            version_list.append(sublist)

        ## create table
        f.write("Total %d\n\n" % len(chart_list))
        print_row(f, [' ']*col, header=True)
        for row in version_list:
            print_row(f, row)

        for chart in chart_list:
            print("- Process %s " % (chart))
            response = get_items(chart_table, filter_value=chart)
            count = len(response)
            limit = args.limit
            f.write("\n### %s\n" % chart)
            f.write("Total: %d  &nbsp;&nbsp;&nbsp;&nbsp;  [source_code](%s)  &nbsp;&nbsp;&nbsp;&nbsp;  [back_to_top](#%s)\n\n" % (
                count, './helmfile/'+chart, 'chart-versions'))
            print_row(f, header_dict.values(), header=True)
            if not count:
                continue
            if count < limit:
                limit = count
            # Sort records by date
            records = sorted(
                response[0:limit],
                key=lambda i: datetime.strptime(
                    i['createTime'], DATETIME_FMT),
                reverse=True
            )
            for record in records:
                data = get_table_data(header_dict.keys(), record)
                print_row(f, data)

    # Reading daat from temp outfile
    with open(tmp_outfile) as fp:
        file_data2 = fp.read()

    # Merging 2 files
    file_data += "\n"
    file_data += file_data2

    # Creating outfile
    with open(OUTFILE, 'w') as fp:
        fp.write(file_data)
    print("- Created output %s" % OUTFILE)


def update_versions(args):
    chart = args.chart_name
    version = args.chart_version
    print(f"Processing chart {chart}:{version}")

    # Get environment variables from pipeline
    pipeline_id = args.pipeline_id if args.pipeline_id else os.getenv(
        "CI_PIPELINE_ID")
    pipeline_url = args.pipeline_url if args.pipeline_url else os.getenv(
        "CI_PIPELINE_URL")
    commit_author = args.commit_author if args.commit_author else os.getenv(
        "CI_COMMIT_AUTHOR")
    commit_title = args.commit_title if args.commit_title else os.getenv(
        "CI_COMMIT_TITLE")
    commit_sha = args.commit_sha if args.commit_sha else os.getenv(
        "CI_COMMIT_SHORT_SHA")
    utc_time = datetime.now(timezone.utc).strftime("%m/%d/%Y, %H:%M:%S UTC")

    # Define configs
    aws_config = Config(region_name="us-east-1")

    # Get dynamo table
    ddb = boto3.resource('dynamodb', config=aws_config)
    chart_table = ddb.Table(DYNAMODB_TABLE)

    # Gather chart information
    id_key = f"{chart}:{version}"
    item_dict = {
        'id': id_key,
        'chart': chart,
        'version': version,
        'CI_PIPELINE_ID': pipeline_id,
        'CI_PIPELINE_URL': pipeline_url,
        'CI_COMMITTER': commit_author,
        'CI_COMMIT_TITLE': commit_title,
        'CI_COMMIT_SHORT_SHA': commit_sha,
        'createTime': utc_time
    }

    print(item_dict)

    # Add item to dynamodb table
    response = chart_table.put_item(
       Item=item_dict
    )

    print(response)
    return response
