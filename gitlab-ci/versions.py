import os
import sys
import argparse

# Add current file dir to path
sys.path.insert(1, os.path.abspath(os.path.dirname(__file__)))
from versions_lib import render_versions, update_versions, cleanup_versions


def main(args):
    print(args)
    if args.actions == "render":
        render_versions(args)
    elif args.actions == "update":
        update_versions(args)
    elif args.actions == "cleanup":
        cleanup_versions(args)
    else:
        print("Action not found!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(
                            dest = 'actions',
                            help='Versioning actions to take')
    subparsers.required = True

    ## RENDER
    render_parser = subparsers.add_parser("render")
    render_parser.add_argument('-c', '--chart_list', dest='chart_list',
                        help="enter list of charts seperated by ','")
    render_parser.add_argument('-l', '--limit', dest='limit', type=int,
                        default=10, help="max versions displayed per chart")

    ## UPDATE
    update_parser = subparsers.add_parser("update")
    update_parser.add_argument('-c', '--chart_name', dest='chart_name',
                        help="enter name of the Helm chart")
    update_parser.add_argument('-v', '--chart_version', dest='chart_version',
                        help="enter version of the Helm chart")
    update_parser.add_argument('-p', '--pipeline_id', dest='pipeline_id')
    update_parser.add_argument('-u', '--pipeline_url', dest='pipeline_url')
    update_parser.add_argument('-a', '--commit_author', dest='commit_author')
    update_parser.add_argument('-s', '--commit_title', dest='commit_title')
    update_parser.add_argument('-t', '--commit_sha', dest='commit_sha')

    ## CLEANUP
    cleanup_parser = subparsers.add_parser("cleanup")
    cleanup_parser.add_argument('-c', '--chart_list', dest='chart_list',
                        help="enter list of charts seperated by ','")
    cleanup_parser.add_argument('-m', '--max', dest='max', type=int,
                        default=10, help="maximum number of chart versions to keep")

    ## Parse arguments
    args = parser.parse_args()
    main(args)
