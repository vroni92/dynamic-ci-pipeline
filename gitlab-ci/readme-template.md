<!--  
DO NOT edit the README.md file directly. Please edit the gitlab-ci/readme-template.md.
-->
# My Project

[[_TOC_]]

## Contributing
### 1. Clone the repo
```bash
git clone git@gitlab.bit9.local:production-engineering/observability/helm-charts/wavefront-proxy.git
```

### 2. Branch off of main
```bash
git checkout main
git pull origin main
git checkout -b feature-branch-name
```

### 3. Make changes
#### A. Update existing Helm chart
Make changes to the files under `helmfile/<chart>/**/*`. If changing a `/helmfile/<chart>/build.sh` script (more details in the [CICD Workflow section](#cicd-workflow)), note that the script is run from the `helmfile/<chart>/` directory.

Update the chart version defined in `helmfile/<chart>/Chart.yaml`.

#### B. Create new Helm chart
Create a new folder under `helmfile/`. The name should match the name of the chart being created. Add the following required files:
##### Required
###### Chart.yaml
```yaml
apiVersion: v2
name: <chart-name> # ex: wavefront-proxy
description: <chart-description>
appVersion: '1.0'
version: <chart-version> # ex: 1.0.0
```

###### values.yaml
```yaml
# DEFAULT VALUES GO HERE
my:
  value: 1
````

##### Optional
###### lint.yaml
```yaml
# The lint.yaml Values file is used template
# the helm chart during CI pipeline execution.
# If not included, the Helm chart will not be
# templated during the pipeline.

# This is a great way to test out value logic
# for your helm chart.
my:
  value: 2
```

###### build.sh
```bash
# The build.sh bash script can be used to run
# custom actions before packaging the Helm chart.
# Check out `helmfile/<chart>/build.sh`
# as an example.

echo "Run my custom build script."
```

Add any other `dependencies` (in the `Chart.yaml`) or `templates` (under `templates/` directory) to create desired resources.

### 4. Install and Run pre-commit
```bash
# Run from top level directory of this repo.
pre-commit install
pre-commit run -a
```

### 5. Rebase and Commit
```bash
# Rebase
git remote -v update
git rebase origin/main

# Commit changes
git commit -am "Commit my changes"
git push origin feature-branch-name
```

### 6. Open an MR
Open an MR to merge your `feature-branch-name` branch into `main`.

Read more about the CI pipelines in the [CICD Workflow section](#cicd-workflow).

This will kick off a Gitlab CI pipeline. The `build` job validates the charts under `helmfile/` and templates them using the values included in `helmfile/*/lint.yaml`. If not lint YAML is included, the chart will not be linted/templated. The jobs only run for charts that have changed.

You are able to push your changes to Harbor for testing by running the `push` job. The chart version will be displayed at the end of the job log.

### 7. Merge MR into main
Merging to `main` will trigger the `push` jobs, which will build and push the charts to Harbor. The version of the charts will be a combination of the chart version defined in `helmfile/*/Chart.yaml` and the short SHA of the commit. For example: `1.8.0-bb31910`

## CICD Workflow
This repo runs CI pipelines on either `Merge Request` events or commits to the `main` branch. Each of these pipelines are dynamically generated, as explained below.

Each pipeline consists of three stages: `rebase` (`Merge Request` only) > `generate-pipeline` > `run-pipeline`.

### Rebase
Be sure to rebase your branch off of the `main` branch, or else this job will fail. See [Rebase and Commit section for details](#rebase-and-commit).

This job is run only on `Merge Request` pipelines.

### Generate Pipeline
This job runs a `python` script (**[generate-pipeline.py](./gitlab-ci/generate-pipeline.py)**) to dynamically create a downstream pipeline based off of the current branch and changes made in the commit. The pipeline is generated off of the template **[pipeline-template.j2](./gitlab-ci/pipeline-template.j2)**.

### Run Pipeline
This job kicks off the downstream pipeline generated in the `generate-pipeline` stage above. This pipeline includes four stages: `package` > `push` > `sync` > `cleanup`.
#### Package
This stage packages each Helm file under `helmfile/` that has changed. There will be one job per chart that has been updated/added.

 If a `lint.yaml` file is included in the chart, this job will run `helm template` using that YAML values file.

 If there is a `build.sh` script included in the chart, this job will run the sript before running `helm package ...`.

 The packaged version will be created from the chart version in `Chart.yaml` and the short commit SHA. For example: `1.0.0-abcdefg`.

 More details on the exact commands run can be found in the `.helm package` template job in **[ci-template.yaml](./gitlab-ci/ci-template.yaml)**.

#### Push
This stages runs the exact same commands as the `package` stage (above), and then pushes the packages Helm chart to Harbor.

This stage is `optional` for Merge Request pipelines. You can kick of this stage to test your new/updated chart functionality before merging your changes to `main`. This is **not required** to run before merging to `main.`

More details on the exact commands run can be found in the `.helm push` template job in **[ci-template.yaml](./gitlab-ci/ci-template.yaml)**.

#### Sync
This stage syncs the versions (packaged and pushed to Harbor in the `push` stage) to a DynamoDB table in the an AWS account. This table stores metadata about the most recent helm Charts created in this repo. It then updates the this `README.md` file to list those chart versions, commit information, etc. This stage uses the `@build` Gitlab user to commit changes to the `main` branch.

Check out the **[versions.py](./gitlab-ci/versions.py)** script for details on the versioning logic used.

This stage is `optional` for Merge Request pipelines, and can only be run if the `push` stage is run.

#### Cleanup
This stage does as it says, cleans up resources created in the previous stages. This includes older charts stored in the DynamoDB table (referenced above) and Helm chart files created during the packaging process.

This stage is `optional` for Merge Request pipelines, and can only be run if the `push` stage is run.


## Trigger Pipeline Manually
To trigger a pipeline for specific Helm Charts manually, you can navigate to `CICD` > `Pipelines` > `Run Pipeline`. Set the variables `DIRECTORIES` to a **space separated list** of charts you want to trigger. For example:
![Manual Pipeline](gitlab-ci/img/manual-pipeline.png)

## Chart Versions
