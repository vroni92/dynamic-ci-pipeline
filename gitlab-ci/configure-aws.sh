#!/usr/bin/env bash
banner() {
    msg="#### $* ####"
    edge=$(echo "$msg" | sed 's/./#/g')
    echo -e "$edge"
    echo -e "$msg"
    echo -e "$edge"
}

configure_assume_role(){
  if [ -d ~/.aws ]; then
    banner AWS config directory exists
    ls ~/.aws
  else
    banner Creating AWS config directory
    mkdir ~/.aws
  fi

  banner Adding AWS profile 'deploy'

  cat <<EOT >> ~/.aws/credentials
[deploy]
aws_access_key_id = $1
aws_secret_access_key = $2
region=$3
EOT

  banner Adding AWS assume role profile 'deploy-role'

  cat <<EOT >> ~/.aws/config
[profile deploy-role]
role_arn = $4
source_profile = deploy
EOT

  export AWS_PROFILE=deploy-role
}

configure_aws(){

  banner Setting environment variables

  export AWS_ACCESS_KEY_ID=$1
  export AWS_SECRET_ACCESS_KEY=$2
  export AWS_DEFAULT_REGION=$3
}

if [ $# -gt 0 ]
then
  # configure_aws $1 $2 $3
  configure_assume_role $1 $2 $3 $4
  aws sts get-caller-identity
  cat ~/.aws/config
else
  banner ERROR: Your command line contains 0 arguments.
  return 1
fi
