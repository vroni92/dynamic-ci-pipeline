echo "Exporting configuration variables..."

export AWS_ACCESS_KEY_ID=XXXX
export AWS_SECRET_ACCESS_KEY=XXXX
export AWS_DEFAULT_REGION=XXXX
export AWS_ASSUME_ROLE_ARN=XXXX
export AWS_EKS_CLUSTER_NAME=XXXX

# Running from top level directory
bash gitlab-ci/configure-aws.sh $AWS_ACCESS_KEY_ID $AWS_SECRET_ACCESS_KEY $AWS_DEFAULT_REGION $AWS_ASSUME_ROLE_ARN
echo "Configure EKS access..."
aws eks --region us-east-1  update-kubeconfig --name $AWS_EKS_CLUSTER_NAME --profile deploy-role
