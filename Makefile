

.PHONY: stage1
stage1:
	@echo "This is stage1 with input $(MY_INPUT)"

.PHONY: stage2
stage2:
	@echo "This is stage2 with input $(MY_INPUT)"

.PHONY: stage3
stage3:
	@echo "This is stage3"

## Example make functions

###########################
### Helmfile Deployments ##
###########################
.PHONY: configure-cluster-access
configure-cluster-access:
	@echo "Configuring access to $(CLUSTER)"
	bash $(BASE_DIRECTORY)/$(CLUSTER)/config.sh

.PHONY: helmfile-diff
helmfile-diff:
	@echo "Running helmfile diff for $(CLUSTER)"
	cd $(BASE_DIRECTORY)/$(CLUSTER)/ && helmfile diff

.PHONY: helmfile-deploy
helmfile-deploy:
	@echo "Running helmfile apply for $(CLUSTER)"
	cd $(BASE_DIRECTORY)/$(CLUSTER)/ && helmfile apply

#############################
### Helm Chart Deployments ##
#############################
REGISTRY_PREFIX?=https://charts.helm.sh/stable
CHART_NAME?=my-chart
CHARTS_DIR?=helmfile
IMAGE_TAG?=$(shell git rev-parse --short HEAD)$(shell if ! git diff-index --quiet HEAD -- ; then echo "-dirty"; fi)
CHART_VERSION=$(shell grep -m1 "version:" ./$(CHARTS_DIR)/${CHART_NAME}/Chart.yaml | sed 's/: /\n/g' | awk 'NR==2')-$(IMAGE_TAG)
CHART_PUBLISH_DIR=./builds
LINT_FILE=./$(CHARTS_DIR)/$(CHART_NAME)/lint.yaml
BUILD_SCRIPT=./$(CHARTS_DIR)/$(CHART_NAME)/build.sh
CHARTS_LIST?=my-chart


.PHONY: package
package:
	helm repo update
	helm dependency update ./$(CHARTS_DIR)/$(CHART_NAME)
	[ -f "$(BUILD_SCRIPT)" ] && { cd ./$(CHARTS_DIR)/$(CHART_NAME)/; echo "Running build.sh..."; bash build.sh; } || echo "$(BUILD_SCRIPT) does not exist."
	[ -f "$(LINT_FILE)" ] && { helm lint --values ./$(CHARTS_DIR)/$(CHART_NAME)/lint.yaml ./$(CHARTS_DIR)/$(CHART_NAME); helm template --values ./$(CHARTS_DIR)/$(CHART_NAME)/lint.yaml ./$(CHARTS_DIR)/$(CHART_NAME); } || echo "$(LINT_FILE) does not exist."
	helm package ./$(CHARTS_DIR)/$(CHART_NAME) --destination $(CHART_PUBLISH_DIR) --version $(CHART_VERSION)


.PHONY: push-chart
push-chart:
	helm version
	helm repo list
	helm cm-push $(CHART_PUBLISH_DIR)/$(CHART_NAME)-$(CHART_VERSION).tgz $(CHART_PUBLISH_REPO) --debug


.PHONY: push-version
update-version:
	python3 gitlab-ci/versions.py update -c $(CHART_NAME) -v $(CHART_VERSION)

.PHONY: render-versions
render-versions:
	python3 gitlab-ci/versions.py render -c $(CHARTS_LIST)

.PHONY: cleanup-versions
cleanup-versions:
	python3 gitlab-ci/versions.py cleanup -c $(CHARTS_LIST) -m 10

.PHONY: clean
clean:
	-rm -rf $(CHART_PUBLISH_DIR)
	-rm -rf helmfile/*/charts helmfile/*/Chart.lock

.PHONY: rebase
rebase:
	git config --global user.email "build@carbonblack.com"
	git config --global user.name "CI-BUILD"
	git clone -b $(CI_MERGE_REQUEST_SOURCE_BRANCH_NAME) https://$(GIT_USER):$(GIT_TOKEN)@gitlab.bit9.local/$(CI_MERGE_REQUEST_SOURCE_PROJECT_PATH) /tmp/$(CI_PROJECT_NAME)
	cd /tmp/$(CI_PROJECT_NAME)
	if [ $(CI_MERGE_REQUEST_SOURCE_PROJECT_URL) == $(CI_MERGE_REQUEST_PROJECT_URL) ]; then \
		git fetch --all; \
		echo "git pull origin $(CI_MERGE_REQUEST_TARGET_BRANCH_NAME) --rebase"; \
		git pull origin $(CI_MERGE_REQUEST_TARGET_BRANCH_NAME) --rebase; \
	else \
	  git remote add upstream https://$(GIT_USER):$(GIT_TOKEN)@gitlab.bit9.local/$(CI_MERGE_REQUEST_PROJECT_PATH); \
		git fetch --all; \
		echo "git pull remote $(CI_MERGE_REQUEST_TARGET_BRANCH_NAME) --rebase"; \
		git pull remote $(CI_MERGE_REQUEST_TARGET_BRANCH_NAME) --rebase; \
	fi
	rm -rf /tmp/$(CI_PROJECT_NAME)


# temp debugging:
# print-%  : ; @echo $* = $($*)

# include helm.mk
